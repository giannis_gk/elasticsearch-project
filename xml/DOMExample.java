package xml;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.json.JSONObject;
import org.json.XML;

import java.nio.charset.StandardCharsets;

public class DOMExample {
	public static void main(String args[]) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		try{
			int count=0;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			File folder = new File("files//"); // all files must be included inside a folder called "files"
			File[] listOfFiles = folder.listFiles();
			for (int j = 0; j < listOfFiles.length; j++) {
				String filename = listOfFiles[j].getName();
				if (listOfFiles[j].isFile()) {
					Document doc = dBuilder.parse(listOfFiles[j]);
					Element rootElement = doc.getDocumentElement();	// get the root element of the document
					NodeList nodes=doc.getChildNodes(); // get the child elements of the document
					for(int i=0;i<nodes.getLength();i++){
						Node ni=nodes.item(i);
						if (ni.getNodeType() == Node.ELEMENT_NODE) {
							count++;
							Element ei=(Element)ni; //type cast to element        	
							//get the content of the fields
							double rcn=Double.parseDouble(ei.getElementsByTagName("rcn").item(0).getTextContent());
							String acronym=ei.getElementsByTagName("acronym").item(0).getTextContent();
							String title=ei.getElementsByTagName("title").item(0).getTextContent();
							String objective=ei.getElementsByTagName("objective").item(0).getTextContent();
							//create new xml element
							String text = title + ". " + objective;
							Element txt = doc.createElement("text");
							txt.appendChild(doc.createTextNode(text));
							rootElement.insertBefore(txt, ei.getElementsByTagName("identifier").item(0)); 
							String identifier=ei.getElementsByTagName("identifier").item(0).getTextContent();
							// remove nodes from xml
							rootElement.removeChild(ei.getElementsByTagName("title").item(0));
							rootElement.removeChild(ei.getElementsByTagName("objective").item(0));					
							// update file
							TransformerFactory transformerFactory = TransformerFactory.newInstance();
							Transformer transformer = transformerFactory.newTransformer();
							DOMSource source = new DOMSource(doc);
							StreamResult result = new StreamResult(new File("files//"+filename));
							transformer.transform(source, result);
						}
						//read xml file
						String line="",str="";
						BufferedReader br = new BufferedReader(new FileReader(listOfFiles[j]));
						while ((line = br.readLine()) != null) {
							str += line;
						}
						//create json file
						JSONObject jsondata = XML.toJSONObject(str);
						File file = new File("output");
						filename = filename.replace(".xml", "");				
						OutputStream outputStream = new FileOutputStream("output//"+filename+".json");
						file.mkdir(); // create directory
						PrintWriter pw = new PrintWriter(new OutputStreamWriter(outputStream, "UTF8"),true); 
						pw.write(jsondata.toString()); 
						pw.flush(); 
						pw.close(); 
					}
				} 
			}
		}
		catch (TransformerException e) { 
			e.printStackTrace();
		}
	}			
}






