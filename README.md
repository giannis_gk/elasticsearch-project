# Elasticsearch project

This project  was created for the course "Information Retrieval".
There is a collection of xml texts and by the use of Elasticsearch, we must retrieve k nearest texts.

Steps:

- Merge the fields "title" and "objectives" to "text".
- Given 10 queries, these queries will be submitted for the field "text".
- Usage of the proper analyzer (stopword removal, lowercase, stemming).
- Usage of the default weighting scheme (BM25).
- Retrieval of the first 20 texts.
 